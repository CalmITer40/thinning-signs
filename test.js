const fs = require("fs");
const { exit } = require("process");
let Network = require("./components/Network");
let utils = require("./components/utils");

let testDs = JSON.parse(fs.readFileSync("files/ds_test.json"), { encoding: "utf-8" });
let weights = JSON.parse(fs.readFileSync("files/weights_new.json"),{ encoding: "utf-8" });
let meta = JSON.parse(fs.readFileSync("files/meta.json"), { encoding: "utf-8" });


// init variables
let cities = [
    'Bryansk',
    'Kaluga'
];
let objects_count = 2;
let x_count = weights[0].length;
let need_b = false;
let x_norm_max = meta.max;
let x_norm_min = meta.min;
let accuracy = 0;

// testing
let nn = new Network(weights, objects_count, [], x_count, need_b, utils.sigmoid, utils.p_sigmoid);
console.log('Тестирование...');
for(let i = 0; i < testDs.length; i++){
    let test = testDs[i].data;
    for(let j = 0; j < x_count; j++){
        test[j] = (test[j] - x_norm_min[j]) / (x_norm_max[j] - x_norm_min[j]);
    }
    let res = nn.query(test);

    console.log('Результат[' + i + ']: ');
    res.forEach((e,idx) => {
        console.log(`${idx} - ${Math.round(e * 100) / 100}%`);
    });

    let index = res.indexOf(Math.max.apply(null, res));
    let arr = res.map((e) => Math.round(e));
    if(testDs[i].out.join() === arr.join()){
        accuracy++;
    }

    console.log('Город: ' + cities[index]);
}
accuracy = accuracy / testDs.length * 100;
console.log('Точность на тестовых данных после прореживания: ' + accuracy + '%');