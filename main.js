const fs = require("fs");
const Network = require("./components/Network");
const utils = require("./components/utils");
const Draw = require("./components/Draw");

let learnDs = JSON.parse(fs.readFileSync("files/ds_learn.json", {
    encoding: "utf-8"
}));
let testDs = JSON.parse(fs.readFileSync("files/ds_test.json"), {
    encoding: "utf-8"
});

let x = [];
let d = [];
let cities = [
    'Bryansk',
    'Kaluga'
];

// preparing data from json
for(let i = 0; i < learnDs.length; i++){
    x.push(learnDs[i].data);
    d.push(learnDs[i].out);
}

// init variables
let objects_count = 2;
let ex_count = x.length;
let x_count = x[0].length;
let need_b = false;
let resultBefore = [];
let weights = [];
let x_norm_max = new Array(x_count).fill(0);
let x_norm_min = new Array(x_count).fill(999);
let x_norm_max_test = new Array(x_count).fill(0);
let x_norm_min_test = new Array(x_count).fill(999);
let matrixSigns = [];
let dt = 0.2;
let accuracy = 0;

// normalization train data
for(let i = 0; i < x_count; i++){
    for(let j = 0; j < ex_count; j++){
        if(x_norm_max[i] < x[j][i]){
            x_norm_max[i] = x[j][i];
        }

        if(x_norm_min[i] > x[j][i]){
            x_norm_min[i] = x[j][i];
        }
    }
}

for(let j = 0; j < x_count; j++){
    for(let i = 0; i < ex_count; i++){
        x[i][j] = (x[i][j] - x_norm_min[j]) / (x_norm_max[j] - x_norm_min[j]);
    }
}

// training
console.log('Обучение...');
let nn = new Network(0, objects_count, [2,2,2], x_count, need_b, utils.sigmoid, utils.p_sigmoid);
console.log('Шагов (эпох) обучения = ', nn.train(x,d));

// saving weights
nn.layers.forEach(l => {
    weights.push(l.w);
});
fs.writeFileSync("files/weights.json", JSON.stringify(weights),err => {});
console.log('Веса сохранены');

// normalization test data
console.log('Тестирование...');
for(let i = 0; i < x_count; i++){
    for(let j = 0; j < testDs.length; j++){
        if(x_norm_max_test[i] < testDs[j].data[i]){
            x_norm_max_test[i] = testDs[j].data[i];
        }

        if(x_norm_min_test[i] > testDs[j].data[i]){
            x_norm_min_test[i] = testDs[j].data[i];
        }
    }
}

// test
for(let i = 0; i < testDs.length; i++){
    let test = testDs[i].data;
    for(let j = 0; j < x_count; j++){
        test[j] = (test[j] - x_norm_min_test[j]) / (x_norm_max_test[j] - x_norm_min_test[j]);
    }
    let res = nn.query(test);
    resultBefore.push([...res]);
    console.log('Результат[' + i + ']: ');
    res.forEach((e,idx) => {
        console.log(`${idx} - ${Math.round(e * 100) / 100}%`);
    });

    let index = res.indexOf(Math.max.apply(null, res));
    let arr = res.map((e) => Math.round(e));
    if(testDs[i].out.join() === arr.join()){
        accuracy++;
    }

    console.log('Город: ' + cities[index]);
}
accuracy = accuracy / testDs.length * 100;
console.log('Точность на тестовых данных: ' + accuracy + '%');

// thinning
console.log('Прореживание...');
for(let i = 0; i < weights.length; i++){
    let layer = [];
    for(let j = 0; j < weights[i].length; j++){
        let neuron = [];

        for(let k = 0; k < weights[i][j].length; k++){
            let buffer = weights[i][j][k];
            weights[i][j][k] = 0;

            let nn_test = new Network(weights, objects_count, [], x_count, need_b, utils.sigmoid, utils.p_sigmoid);

            let next = [];
            for(let l = 0; l < testDs.length; l++){
                let test = testDs[l].data;
                let res = nn_test.query(test);
                next.push([...res]);
            }

            let quality = isLowQuality(next, resultBefore);
            if(quality.flag){
                weights[i][j][k] = buffer;
            }

            neuron.push(quality);
        }

        layer.push(neuron);
    }

    matrixSigns.push(layer);
}

// saving new weights
fs.writeFileSync("files/weights_new.json", JSON.stringify(weights),err => {});
console.log('Обновленные веса сохранены');
fs.writeFileSync("files/meta.json", JSON.stringify({ max: x_norm_max_test, min: x_norm_min_test, matrix: matrixSigns}),err => {});

// print result
Draw.render(matrixSigns, testDs.length, dt);

// check quality
function isLowQuality(set, canon){
    let res = {
        flag: 0,
        pos: 0,
        neg: 0
    };

    for(let i = 0; i < canon.length; i++){
        for(let j = 0; j < canon[i].length; j++){
            if(Math.abs(canon[i][j] - set[i][j]) > dt){
                if(!res.flag){
                    res.flag = 1;
                }
            }
        }

        let first = canon[i].map(e => e >= 0.5 ? 1 : 0);
        let second = set[i].map(e => e >= 0.5 ? 1 : 0);

        if(first.join() === second.join()){
            res.pos++;
        }else{
            res.neg++;
        }
    }

    return res;
}
