const sigmoid = 1;
const p_sigmoid = 101;
const th = 2;
const p_th = 102;

function f_sigmoid(y) {
    return 1 / (1 + Math.exp(-y));
}

function p_f_sigmoid(y) {
    return y * (1 - y);
}

function f_th(y) {
    return (Math.exp(y) - Math.exp(-y)) / (Math.exp(y) + Math.exp(-y));
}

function p_f_th(y) {
    return 1 - Math.pow(y, 2);
}

function f_activation(id, y) {
    if (id == sigmoid) {
        y = f_sigmoid(y);
    } else if (id == th) {
        y = f_th(y);
    }

    return y
}

function p_f_activation(id, y) {
    if (id == p_sigmoid) {
        y = p_f_sigmoid(y);
    } else if (id == p_th) {
        y = p_f_th(y);
    }

    return y;
}

function neuron_y(x, w, f_id, b = null) {
    let v = 0;
    for (let i = 0; i < x.length; i++) {
        v += x[i] * w[i];
    }

    if (b == null) {
        y = f_activation(f_id, v);
    } else {
        y = f_activation(f_id, v + b);
    }
    return y;
}


module.exports = {
    sigmoid,
    p_sigmoid,
    th,
    p_th,
    f_sigmoid,
    p_f_sigmoid,
    p_f_th,
    f_activation,
    p_f_activation,
    neuron_y
};