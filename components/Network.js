let Layer = require('./Layer');
let utils = require("./utils");

class Network{
    constructor(params, objects_count, params_layers, x_count, need_b, f_id, p_f_id){
        if(params === 0){
            this.layers = [];
            this.p_f_id = p_f_id;
            this.layers_count = params_layers.length;
    
            for(let i = 0; i < params_layers.length; i++){
                if (i === 0){
                    this.layers.push(new Layer(0,params_layers[i], x_count, need_b, f_id));
                }else if(i === this.layers_count - 1){
                    this.layers.push(new Layer(0,objects_count, params_layers[i-1], need_b, f_id));
                }else{
                    this.layers.push(new Layer(0,params_layers[i], params_layers[i-1], need_b, f_id));
                }
            }
    
            this.e = 0.1;
            this.r = 0.05;
            this.t_count = 0;
            this.t_max_count = 50000;
        }else{
            this.layers = [];
            this.p_f_id = p_f_id;
            this.layers_count = params.length;
    
            for(let i = 0; i < params.length; i++){
                if (i === 0){
                    this.layers.push(new Layer(params[i],params[i][0].length, params[i].length, need_b, f_id));
                }else if(i === this.layers_count - 1){
                    this.layers.push(new Layer(params[i],params[i][0].length, params[i-1][0].length, need_b, f_id));
                }else{
                    this.layers.push(new Layer(params[i],params[i][0].length, params[i-1][0].length, need_b, f_id));
                }
            }
        }   
    }

    query(x){
        for(let i = 0; i < this.layers_count; i++){
            if(i === 0){
                this.layers[i].query(x);
            }else{
                this.layers[i].query(this.layers[i-1].y);
            }
        }

        return this.layers[this.layers_count-1].y;
    }

    train(x, d){
        let flag = true;
        while (flag){
            if (this.t_count > this.t_max_count){
                break;
            }
        
            flag = false;

            for(let i = 0; i < x.length; i++){
                let y = this.query(x[i]);

                let need_train = false;

                for(let j = 0; j < y.length; j++){
                    if(Math.abs(d[i][j] - y[j]) > this.e){
                        flag = true;
                        need_train = true;
                        break;
                    }
                }

                if(need_train){
                    let g = null;
                    let g_pred = null;

                    for(let l = this.layers_count - 1; l >= 0; l--){
                        g = new Array(this.layers[l].neurons).fill(0);
                        for(let j = 0; j < this.layers[l].neurons; j++){
                            if(l === this.layers_count - 1){
                                g[j] = utils.p_f_activation(this.p_f_id, this.layers[l].y[j]) * (d[i][j] - this.layers[l].y[j]);
                            }else{
                                let temp = 0;
                                for(let k = 0; k < this.layers[l+1].neurons; k++){
                                    temp += g_pred[k] * this.layers[l+1].w[j][k];
                                }
                                g[j] = utils.p_f_activation(this.p_f_id, this.layers[l].y[j]) * temp;
                            }

                            for(let k = 0; k < this.layers[l].x_count; k++){
                                if(l === 0){
                                    this.layers[l].w[k][j] += this.r * g[j] * x[i][k];
                                }else{
                                    this.layers[l].w[k][j] += this.r * g[j] * this.layers[l-1].y[k];
                                }
                            }

                            if(this.layers[l].b != null){
                                this.layers[l].b[j] += this.r * g[j];
                            }
                        }

                        g_pred = g;
                    }
                }
            }

            this.t_count++;
        }

        return this.t_count;
    }
}

module.exports = Network;