const fs = require("fs");
const {createCanvas} = require("canvas");

class Draw{
    static render(matrix, test_count, dt){
        if(!matrix.length){
            return false;
        }


        let left = 0;
        let top = 0;
        let width = 100;
        let height = 50;
        let size = {
            width: width + (matrix.length * matrix[0].length) * width,
            height: height + matrix[0][0].length * height + 250
        };
        let less = 0;


        let canvas = createCanvas(size.width, size.height);
        let ctx = canvas.getContext("2d");
        ctx.fillStyle = 'white';
        ctx.fillRect(0,0,size.width, size.height);

        drawCell(left, top, width, height, {
            fill: 'white',
            stroke: 'black'
        }, 'Нейрон/связь');

        left += width;

        for(let i = 0; i < matrix.length; i++){
            for(let j = 0; j < matrix[i].length; j++) {
                drawCell(left, top, width, height, {
                    fill: 'white',
                    stroke: 'black'
                },j + 1);
                left += width;
            }
        }
        top += height;
        left = 0;

        for(let i = 0; i < matrix[0].length; i++){
            drawCell(left, top, width, height, {
                fill: 'white',
                stroke: 'black'
            }, i + 1);
            left += width;

            for (let j = 0; j < matrix.length; j++){
                for(let k = 0; k < matrix[j].length; k++) {
                    drawCell(left, top, width, height, {
                        fill: matrix[j][i][k].flag ? 'yellow' : 'green',
                        stroke: 'black'
                    }, 'P-' + matrix[j][i][k].pos + ' N-' + matrix[j][i][k].neg);

                    if(!matrix[j][i][k].flag){
                        let temp = matrix[j][i][k].neg / test_count * 100;
                        if(temp > less){
                            less = temp;
                        }
                    }

                    left += width;
                }
            }
            left = 0;
            top += height;
        }

        ctx.font = "16px serif";

        ctx.fillStyle = 'yellow';
        ctx.fillRect(left + 20, top + 15, 20, 20);
        ctx.fillStyle = 'black';
        ctx.fillText(' - связь обязательна', left + 50, top += 30);
        ctx.fillStyle = 'green';
        ctx.fillRect(left + 20, top + 15, 20, 20);
        ctx.fillStyle = 'black';
        ctx.fillText(' - связь необязательна', left + 50, top += 30);

        ctx.fillStyle = 'black';
        ctx.fillText('P - кол-во верно распознанных при удалении связи', left + 20, top += 30);
        ctx.fillText('N - кол-во неверно распознанных при удалении связи', left + 20, top += 30);

        ctx.fillText('Тестовых данных - ' + test_count, left + 20, top += 30);
        ctx.fillText('Задана допустимая погрешность - ' + dt, left + 20, top += 30);
        ctx.fillText('Текущий процент ошибок - ' + less + '%', left + 20, top += 30);

        const out = fs.createWriteStream('files/result.png');
        const stream = canvas.createPNGStream();
        stream.pipe(out);
        out.on('finish', () =>  console.log('Результат сохранен в файле result.png'));

        function drawCell(x,y,w,h,style,value) {
            ctx.fillStyle = style.fill;
            ctx.strokeStyle = style.stroke;

            ctx.fillRect(x, y, w, h);
            ctx.strokeRect(x, y, w, h);

            ctx.font = "12px serif";
            ctx.fillStyle = "black";
            ctx.fillText(value, x + 10, y + height / 2 + 6)
        }
    }
}

module.exports = Draw;