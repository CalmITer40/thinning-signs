let utils = require("./utils");

class Layer{
    constructor(weights,neurons, x_count, need_b, f_id) {
        if(weights === 0){
            this.neurons = neurons;
            this.x_count = x_count;
            this.w = new Array(x_count);
            for(let i = 0; i < this.w.length; i++){
                this.w[i] = new Array(this.neurons);
    
                for(let j = 0; j < this.neurons; j++){
                    this.w[i][j] = Layer.random();
                }
            }
    
            if(need_b){
                this.b = new Array(this.neurons);
                for(let i = 0; i < this.b.length; i++){
                    this.b[i] = Layer.random();
                }
            }
    
            this.f_id = f_id;
            this.y = new Array(this.neurons).fill(0);
        }else{
            this.neurons = neurons;
            this.x_count = x_count;
            this.w = weights;
            this.f_id = f_id;
            this.y = new Array(this.neurons).fill(0);
        }
    } 

    query(x){
        if(this.b == null){
            for(let i = 0; i < this.neurons; i++){

                let ww = [];
                for(let j = 0; j < this.x_count; j++){
                    ww.push(this.w[j][i]);
                }

                this.y[i] = utils.neuron_y(x,ww,this.f_id);
            }
        }else{
            for(let i = 0; i < this.neurons; i++){
                let ww = [];
                for(let j = 0; j < this.x_count; j++){
                    ww.push(this.w[j][i]);
                }

                this.y[i] = utils.neuron_y(x,ww,this.f_id,this.b[i]);
            }
        }
    }

    static random(min = -0.1, max = 0.1){
        return min + Math.random() * (max - min);
    } 
}

module.exports = Layer;