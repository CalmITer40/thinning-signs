# Структура проекта

- **components**
    - *Draw.js*
    - *Layer.js*
    - *Network.js*
    - *utils.js*
- **files** 
    - ds_learn.json
    - ds_test.json
    - meta.json
    - result.png
    - weights.json
    - weigths_new.json
- *main.js*
- *test.js*

#### Для запуска обучения выполнить `node main.js`
#### Для запуска повторного тестирования выполнить `node test.js`